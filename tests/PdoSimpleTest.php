<?php

namespace IleafCtg\PdoSimpleTest;

use Aura\Sql\ExtendedPdo;
use Exception;
use IleafCtg\PdoSimple\PdoSimple;
use PHPUnit\Framework\TestCase;

/**
 *
 * Tests related to CRUD operations to a database.
 *
 */
final class PdoSimpleTest extends TestCase
{
    
    protected $test_table = 'unit_testing';
    
    /** @var PdoSimple */
    protected $pdoSimple;
    
       
    protected function newPdo()
    {
        return new PdoSimple(new ExtendedPdo('sqlite::memory:'));
    }
    
    protected function createTable()
    {
        $stm = "CREATE TABLE `{$this->test_table}` (
          `id` INTEGER PRIMARY KEY AUTOINCREMENT,
          `name` varchar(60) NOT NULL DEFAULT '',
          `value` varchar(60) NOT NULL DEFAULT '',
          `ctime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
        )";

        $this->pdoSimple->getPdoInstance()->exec($stm);
    }
    
    protected function fillTable()
    {
        // Add some rows of data.
        $insert_stmt = "INSERT INTO {$this->test_table} (`name`, `value`) VALUES ('a', 1)";
        $this->pdoSimple->getPdoInstance()->perform($insert_stmt);
    
        $insert_stmt = "INSERT INTO {$this->test_table} (`name`, `value`) VALUES ('b', 2)";
        $this->pdoSimple->getPdoInstance()->perform($insert_stmt);
    
        $insert_stmt = "INSERT INTO {$this->test_table} (`name`, `value`) VALUES ('c', 3)";
        $this->pdoSimple->getPdoInstance()->perform($insert_stmt);
    
        $insert_stmt = "INSERT INTO {$this->test_table} (`name`, `value`) VALUES ('d', 4)";
        $this->pdoSimple->getPdoInstance()->perform($insert_stmt);
    }
    
    /**
     * Establish test environment before each test.
     */
    protected function setUp() : void
    {
        if (! extension_loaded('pdo_sqlite')) {
            $this->markTestSkipped("Need 'pdo_sqlite' to test in memory.");
        }
    
        $this->pdoSimple = $this->newPdo();
    
        $this->createTable();
        $this->fillTable();
    }
    
    
    /**
     * Cleanup after each test.
     */
    protected function tearDown() : void
    {
        // Clear out our test_data table
        $del_statement = "DELETE FROM {$this->test_table}";
        $this->pdoSimple->getPdoInstance()->perform($del_statement);
    }
    
    
    /**
     * Test dbGetRecord().
     * 
     * @throws Exception
     */
    public function test_db_get_record()
    {
        $record = $this->pdoSimple->dbGetRecord($this->test_table, ['value' => 1]);
        
        $this->assertEquals('a', $record['name']);
        
    }
    
    
    /**
     * Test dbInsertRecord(), dbGetRecord(), dbUpdateRecord(), dbUpsertRecord(), and dbDeleteRecord()
     * 
     * @throws Exception
     */
    public function test_db_crud()
    {
        $value = 'myvalue' . rand(1,1000);
        $record = [
            'name' => 'mytest1',
            'value' => $value
        ];
        
        // Insert
        $count = $this->pdoSimple->dbInsertRecord($this->test_table, $record);
        $insert_id = $this->pdoSimple->getLastInsertId();

        // Insert another, so we have two records for our delete_where() test later.
        $record['name'] = 'mytest2';
        $record['value'] = 'inserted';
        $secondCount = $this->pdoSimple->dbInsertRecord($this->test_table, $record);
        $insert_id_two = $this->pdoSimple->getLastInsertId();
        
        
        // Get
        $record = $this->pdoSimple->dbGetRecord($this->test_table, ['id' => $insert_id]);
        
        $this->assertEquals(1, $count, "Insert didn't work, expected 1 row inserted, but got count of {$count} back");
        $this->assertEquals($value, $record['value']);
        
        // Check our select(), selectWhere() methods
        $record = $this->pdoSimple->select('id, name', $this->test_table, ['id' => $insert_id]);
        $this->assertEquals(1, count($record), "Insert didn't work, expected 1 row inserted, but got count of {$count} back");
        $this->assertEquals('mytest1', $record[0]['name']);
        
        $record = $this->pdoSimple->selectWhere('id, name', $this->test_table, "id = :id AND value = :value", ['id' => $insert_id, 'value' => $value]);
        $this->assertEquals(1, count($record), "Insert didn't work, expected 1 row inserted, but got count of {$count} back");
        $this->assertEquals('mytest1', $record[0]['name']);
        
        
        // Update
        $count = $this->pdoSimple->dbUpdateRecord($this->test_table, ['id' => $insert_id], ['value' => 'testvalue']);
        $record = $this->pdoSimple->dbGetRecord($this->test_table, ['id' => $insert_id]);

        $this->assertEquals(1, $count, "Update didn't work, expected 1 row updated, but got count of {$count} back");
        $this->assertEquals('testvalue', $record['value']);

        // Upsert - test that insert when there's a primary key collision will result in an update.
        $count = $this->pdoSimple->dbUpsertRecord($this->test_table, ['id' => $insert_id, 'name' => 'test upsert', 'value' => 'whatever']);
        $record = $this->pdoSimple->dbGetRecord($this->test_table, ['id' => $insert_id]);
        // NOTE: For MySQL, the affectedRows count would be 2 in this scenario.  They say: "With ON DUPLICATE KEY UPDATE,
        //       the affected-rows value per row is 1 if the row is inserted as a new row, 2 if an existing row is updated,
        //       and 0 if an existing row is set to its current values."
        //       Not sure why, other than it makes it possible to know whether your upsert truly updated an existing row
        //       or inserted a new one.
        // For our unit test, since we're using SQLite, it's more akin to PostgreSQL, so the count is 1 regardless.
        $this->assertEquals(1, $count, "Upsert didn't work, expected 1 row updated, but got count of {$count} back");
        $this->assertEquals('whatever', $record['value']);
        $this->assertEquals('test upsert', $record['name']);
        
        // Upsert - test that insert when no primary key collision results in insert.
        $count = $this->pdoSimple->dbUpsertRecord($this->test_table, ['name' => 'test upsert insert', 'value' => 'inserted']);
        $insert2_id = $this->pdoSimple->getPdoInstance()->lastInsertId();
        $record = $this->pdoSimple->dbGetRecord($this->test_table, ['id' => $insert2_id]);
        $this->assertEquals(1, $count, "Upsert didn't work, expected 1 row inserted, but got count of {$count} back");
        $this->assertEquals('inserted', $record['value']);
        $this->assertEquals('test upsert insert', $record['name']);


        // Delete
        $count = $this->pdoSimple->dbDeleteRecord($this->test_table, $insert_id);
        $record = $this->pdoSimple->dbGetRecord($this->test_table, ['id' => $insert_id]);
        $this->assertEquals(1, $count, "Delete didn't work, expected 1 row deleted, but got count of {$count} back");
        $this->assertEmpty($record);

        // Delete Multiple
        $count = $this->pdoSimple->dbDeleteRecords($this->test_table, ['value' => 'inserted']); // Should be 2 of these records now.
        $record = $this->pdoSimple->dbGetRecord($this->test_table, ['id' => $insert2_id]);
        $record2 = $this->pdoSimple->dbGetRecord($this->test_table, ['id' => $insert_id_two]);
        $this->assertEquals(2, $count, "Delete didn't work, expected 2 rows deleted, but got count of {$count} back");
        $this->assertEmpty($record);
        $this->assertEmpty($record2);
    
        
        
        // Insert some more so we can test some other deletes.
        $value = 'myvalue' . rand(1,1000);
        $record = [
            'name' => 'mytest1',
            'value' => $value
        ];
        $count = $this->pdoSimple->dbInsertRecord($this->test_table, $record);
        $insert_id = $this->pdoSimple->getLastInsertId();
        static::assertEquals(1, $count);
        
        // Insert another, so we have two records for our delete_where() test later.
        $record['name'] = 'mytest2';
        $record['value'] = $value;
        $secondCount = $this->pdoSimple->dbInsertRecord($this->test_table, $record);
        $insert_id_two = $this->pdoSimple->getLastInsertId();
        static::assertEquals(1, $secondCount);
    
        // Delete Multiple
        $count = $this->pdoSimple->dbDeleteRecordsWhere($this->test_table, "`value` = :value", ['value' => $value]); // Should be 2 of these records now.
        $record = $this->pdoSimple->dbGetRecord($this->test_table, ['id' => $insert_id]);
        $record2 = $this->pdoSimple->dbGetRecord($this->test_table, ['id' => $insert_id_two]);
        $this->assertEquals(2, $count, "Delete didn't work, expected 2 rows deleted, but got count of {$count} back");
        $this->assertEmpty($record);
        $this->assertEmpty($record2);
    }
    
    
    
    /**
     * Test bulk upserts
     */
    public function test_db_bulk_upserts()
    {
        $value1 = 'myvalue' . rand(1, 1000);
        $value2 = 'myvalue' . rand(1, 1000);
        $value3 = 'myvalue' . rand(1, 1000);
        
        $records = [
            [
                'id'    => 101,
                'name'  => 'mytest1',
                'value' => $value1
            ],
            [
                'id'    => 102,
                'name'  => 'mytest2',
                'value' => $value2
            ],
            [
                'id'    => 103,
                'name'  => 'mytest3',
                'value' => $value3
            ],
        ];
    
        // Test a multiple record upsert
        $count = $this->pdoSimple->dbUpsertRecords($this->test_table, $records);
        $this->assertEquals(3, $count);
        
        // Perform again, and this time it should result in zero records, because it'll be an update but nothing changed.
        // NOTE: This is only true for MySQL.  If it's PostgreSQL or SQLite, they incremente the counter regardless, apparently.
        $count = $this->pdoSimple->dbUpsertRecords($this->test_table, $records);
        $this->assertEquals(3, $count);
        
        // Modify the values now, and perform again.  And this time we should have 6 records updated, because it does a delete/insert to replace the rows (I think).
        $records[0]['value'] .= "modified";
        $records[1]['value'] .= "modified";
        $records[2]['value'] .= "modified";
        $count = $this->pdoSimple->dbUpsertRecords($this->test_table, $records);
        // NOTE: here as well there's a difference between MySQL and PostgreSQL and SQLite.  MySQL would have a count of 6, while the others have a count of 3
        $this->assertEquals(3, $count);
        
    }
    
    
}

