<?php

namespace IleafCtg\PdoSimple;

use Aura\Sql\ExtendedPdo;
use Aura\Sql\ExtendedPdoInterface;
use Exception;

class PdoSimple
{
    
    private static $instance;
    
    /** @var ExtendedPdo */
    private $pdo;
    
    
    
    /**
     * PdoSimple constructor.
     *
     * @param ExtendedPdoInterface|null $pdo
     */
    public function __construct(ExtendedPdoInterface $pdo = null)
    {
        if (!empty($pdo)) {
            $this->pdo = $pdo;
        }
    }
    
    
    
    /**
     * A singleton factory helper - returns the same instance of this class every time it is called.
     * @return PdoSimple
     */
    public static function instance(): PdoSimple
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    
    
    /**
     * Returns the instace of ExtendedPdo associated with this object.
     *
     * @return null|ExtendedPdo
     */
    public function getPdoInstance()
    {
        return $this->pdo;
    }
    
    
    
    /**
     * Initialize the database handle we'll use for all of our SQL queries/inserts/updates/deletes.
     *
     * @param $host
     * @param $port
     * @param $dbname
     * @param $username
     * @param $password
     */
    public function setConnection($host, $port, $dbname, $username, $password)
    {
        // Blow away our current connection, if present.
        if ($this->pdo) {
            unset($this->pdo);
        }
        
        $driver = 'mysql';
        $dsn = "{$driver}:host={$host};port={$port};dbname={$dbname}";
        
        $this->pdo = new ExtendedPdo(
            $dsn,
            $username,
            $password,
            [], // driver attributes/options as key-value pairs
            []  // queries to execute after connection
        );
    }
    
    
    
    /**
     * Validates whether our PDO connection is set. Simple check of whether it's null or not.
     *
     * @return bool   True if set, false otherwise.
     */
    private function validatePdoConnection(): bool
    {
        if (is_null($this->pdo)) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Retrieve a single record from a table.  If more than one record would be returned based on the filters,
     * an exception is thrown.
     *
     * @param string $table  Table to retrieve the record from.
     * @param array  $where  Array of col => value pairs to use to filter on when retrieving the record.
     *
     * @return array      The record as a keyed array.
     * @throws Exception
     */
    public function dbGetRecord(string $table, array $where) : array
    {
        // Get the record.
        $results = $this->select('*', $table, $where, ['limit' => 2]);
        
        // If we got more than one response, they shouldn't have used this function to do so.
        if (count($results) > 1) {
            throw new Exception("Expected a single result but query returned more - modify your query and try again.");
        }
        
        if (!empty($results)) {
            return $results[0];
        }
        return [];
    }
    
    
    /**
     * Retrieve records from a table.
     *
     * @param string $select_list  What to select from the DB
     * @param string $table        Table to retrieve the record from.
     * @param string $where_string A SQL fragment with variables marked with :var
     * @param array  $where_params Array of col => value pairs to replace into the SQL fragment
     *
     * @param array  $options      Recognized options: group_by, order_by, limit, offset
     *
     * @return array      The record as a keyed array.
     * @throws Exception
     */
    public function selectWhere(
        string $select_list,
        string $table,
        string $where_string,
        array $where_params,
        array $options = []
    ): array {
        if (!$this->validatePdoConnection()) {
            throw new Exception("Database connection not initialized.");
        }
        if (!is_string($select_list) or !is_string($table) or !is_string($where_string)) {
            throw new Exception("\$where_string should be a string!");
        }
        if (!is_array($where_params)) {
            throw new Exception("\$where_params should be an array!");
        }
        
        // Initialize params if not set.
        foreach (['group_by', 'order_by', 'limit', 'offset'] as $option_param) {
            if (!isset($options[$option_param])) {
                switch ($option_param) {
                    case 'limit':
                    case 'offset':
                        $options[$option_param] = 0;
                        break;
                    default:
                        $options[$option_param] = '';
                        break;
                }
            }
        }
        
        $group_by = $this->createGroupByClause($options['group_by']);
        $order_by = $this->createOrderByClause($options['order_by']);
        $limit = $this->createLimitClause($options['limit'], $options['offset']);
        
        // Do the SQL query
        $sql = "SELECT {$select_list} FROM {$table} WHERE {$where_string} {$group_by} {$order_by} {$limit}";
        return $this->pdo->fetchAll($sql, $where_params);
    }
    
    /**
     * Create an GROUP BY clause from given options.
     *
     * @param string $group_by The column(s) to group result set by.
     *
     * @return string           The GROUP BY to append to a statement.
     */
    private function createGroupByClause(string $group_by): string
    {
        if ($group_by) {
            return "GROUP BY " . static::quoteWithBackticks($group_by);
        }
        return '';
    }
    
    
    
    /**
     * Takes a string and ensures that all pieces of it have been enclosed in backticks and separated by commas.
     *
     * @param string $params
     *
     * @return string
     */
    private function quoteWithBackticks(string $params = ''): string
    {
        $result = '';
        if ($params) {
            // Ensure we have no more than one space or comma anywhere.
            $params = preg_replace('![\s,]+!', ',', $params);
            // Strip out any existing backticks, we're going to place our own.
            $params = preg_replace('!`+!', '', $params);
            // Convert to an array for easy processing.
            $params = explode(",", $params);
            
            $result = '';
            foreach ($params as $param) {
                if (strlen($result) > 0) {
                    $result .= ',';
                }
                
                $result .= "`{$param}`";
            }
        }
        
        return $result;
    }
    
    /**
     * Create an ORDER BY clause from given options.
     *
     * @param string $order_by The column(s) to order result set by.
     *
     * @return string           The ORDER BY to append to a statement.
     */
    private function createOrderByClause(string $order_by): string
    {
        if ($order_by) {
            return "ORDER BY " . static::quoteWithBackticks($order_by);
        }
        return '';
    }
    
    /**
     * Create a LIMIT modifier for a SELECT statement given the row/offset values.
     *
     * @param int $rows   Number of rows requested.
     * @param int $offset Offset into dataset.
     *
     * @return string
     */
    private function createLimitClause(int $rows, int $offset): string
    {
        $clause = '';
        if ($rows > 0) {
            if ($offset > 0) {
                $clause = " LIMIT {$offset}, {$rows}";
            } else {
                $clause = " LIMIT 0, {$rows}";
            }
        }
        
        return $clause;
    }
    
    
    
    /**
     * Retrieve records from a table.
     *
     * @param string $select_list What to select from the DB
     * @param string $table       Table to retrieve the record from.
     * @param array  $where_array Array of col => value pairs to construct a WHERE clause from.
     *
     * @param array  $options     Recognized options: group_by, order_by, limit, offset
     *
     * @return array      The records as an array of keyed arrays.
     * @throws Exception
     */
    public function select(string $select_list, string $table, array $where_array, array $options = []): array
    {
        // Build our where string.
        $where_str_arr = [];
        foreach (array_keys($where_array) as $col) {
            $where_str_arr[ ] = "`{$col}` = :{$col}";
        }
        $where_str = implode(" AND ", $where_str_arr);
        
        return $this->selectWhere($select_list, $table, $where_str, $where_array, $options);
    }
    
    
    
    /**
     * Delete a single record from a table.
     *
     * @param string $table name of table you want to insert into.
     * @param int    $id    Id of the record you want to delete.
     *
     * @return int   Number of affected records.
     * @throws Exception
     */
    public function dbDeleteRecord(string $table, int $id): int
    {
        if (is_string($this->pdo)) {
            throw new Exception("Database connection not initialized.");
        }
        
        $record = $this->dbGetRecord($table, ['id' => $id]);
        
        $count = 0;
        if ($record) {
            $stm = "DELETE FROM `{$table}` WHERE id = :id";
            $count = $this->pdo->fetchAffected($stm, ['id' => $id]);
        }
        
        return $count;
    }
    
    
    
    /**
     * Delete multiple records from a table.
     *
     * @param string $table
     * @param array  $where
     *
     * @return int
     * @throws Exception
     */
    public function dbDeleteRecords(string $table, array $where): int
    {
        // First, do a select to get all of the to-be-affected ids.
        $records = $this->select('id', $table, $where);
        
        $ids = array_column($records, 'id');
        
        $count = 0;
        if ($ids) {
            $stm = "DELETE FROM `{$table}` WHERE id IN (:ids)";
            $count = $this->pdo->fetchAffected($stm, ['ids' => $ids]);
        }
        
        return $count;
    }
    
    
    
    /**
     * Delete multiple records from a table.
     *
     * @param string $table
     * @param string $where_string
     * @param array  $where_params
     *
     * @return int
     * @throws Exception
     */
    public function dbDeleteRecordsWhere(string $table, string $where_string, array $where_params): int
    {
        // First, do a select to get all of the to-be-affected ids.
        $records = $this->selectWhere('id', $table, $where_string, $where_params);
        
        $ids = array_column($records, 'id');
        
        $count = 0;
        if ($ids) {
            $stm = "DELETE FROM `{$table}` WHERE id IN (:ids)";
            $count = $this->pdo->fetchAffected($stm, ['ids' => $ids]);
        }
        
        return $count;
    }
    
    
    /**
     * Returns the last inserted autoincrement sequence value.
     *
     * @return int
     */
    public function getLastInsertId(): int
    {
        return $this->pdo->lastInsertId();
    }
    
    /**
     * Inserts a single record into a table.
     *
     * @param string $table   name of table you want to insert into.
     * @param array  $data    key => value pairs of record you'd like to insert.
     * @param bool   $replace If true, perform a REPLACE INTO instead of an INSERT INTO
     *
     * @return int  Number of affected records. In this case it should always be 1.
     * @throws Exception
     */
    public function dbInsertRecord(string $table, array $data, bool $replace = false): int
    {
        if (!$this->validatePdoConnection()) {
            throw new Exception("Database connection not initialized.");
        }
        
        $cols = array_keys($data);
        $vals = [];
        foreach ($cols as $col) {
            $vals[] = ":{$col}";
        }
        $cols = "`" . implode('`, `', $cols) . "`";
        $vals = implode(', ', $vals);
        
        $action = $replace ? "REPLACE" : "INSERT";
        $stm = "{$action} INTO `{$table}` ({$cols}) VALUES ({$vals})";
        
        $count = $this->pdo->fetchAffected($stm, $data);
        
        // We should always have 1 row, and sometimes more if it's a REPLACE INTO
        if ($count < 1) {
            throw new Exception("Expected to insert 1 entry into {$table}, but inserted {$count}");
        }
        
        return $count;
    }
    
    /**
     * Update a single record in a table.
     *
     * @param string $table name of table you want to update a record from.
     * @param array  $where Array of col => value pairs to use to filter on when retrieving the record.
     * @param array  $data  key => value pairs of record you'd like to update.
     *
     * @return int  Number of affected records. In this case it should always be 1 or 0.
     * @throws Exception
     */
    public function dbUpdateRecord(string $table, array $where, array $data): int
    {
        if (!$this->validatePdoConnection()) {
            throw new Exception("Database connection not initialized.");
        }
        
        // Make sure we can do this safely and get 1 record back.
        $record = $this->dbGetRecord($table, $where);
        if (!$record) {
            throw new Exception("Record doesn't exist, can't update");
        }
        
        // Build our where string.
        $where_str_arr = [];
        foreach (array_keys($where) as $col) {
            $where_str_arr[ ] = "`{$col}` = :{$col}";
        }
        $where_str = implode(" AND ", $where_str_arr);
        
        // Build our SET string
        $set_str_arr = [];
        foreach (array_keys($data) as $col) {
            $set_str_arr[ ] = "`{$col}` = :{$col}_data"; // Add '_data' as a suffix to ensure we won't have bindings conflicts if we're using the same column name in both our where clause and our SET portion of the query.
        }
        $set_str = implode(", ", $set_str_arr);
        
        $stm = "UPDATE `{$table}` SET {$set_str} WHERE {$where_str}";
        
        // It is a problem if we have the same key in both the $where and $data arrays,
        // so we'll create a data_bindings array that forces a uniquely namespaced key for each item.
        $data_bindings = [];
        foreach ($data as $key => $val) {
            $data_bindings[$key."_data"] = $val;
        }
        $bindings = array_merge($where, $data_bindings);
        
        $count = $this->pdo->fetchAffected($stm, $bindings);
        if ($count > 1) {
            throw new Exception("Expected to update 0 or 1 entry in {$table}, but updated {$count}");
        }
        
        return $count;
    }
    
    
    
    /**
     * Performs an INSERT .. ON DUPLICATE KEY UPDATE statement.
     *
     * @param string $table name of table you want to insert into.
     * @param array  $data  key => value pairs of record you'd like to insert.
     *
     * @return int  Number of affected records. In this case it should always be 1 (insert) or 2 (update).
     * @throws Exception
     */
    public function dbUpsertRecord(string $table, array $data): int
    {
        if (!$this->validatePdoConnection()) {
            throw new Exception("Database connection not initialized.");
        }
        
        // Build our INSERT portion of the statement.
        $cols = array_keys($data);
        $vals = array();
        foreach ($cols as $col) {
            $vals[] = ":$col";
        }
        $cols = "`" . implode('`, `', $cols) . "`";
        $vals = implode(', ', $vals);
        
        $stm = "INSERT INTO `{$table}` ({$cols}) VALUES ({$vals})";
        
        /*
         * Example syntax for MySQL
         *
                INSERT INTO books
                    (id, title, author, year_published)
                VALUES
                    (@id, @title, @author, @year_published)
                ON DUPLICATE KEY UPDATE
                    title = @title,
                    author = @author,
                    year_published = @year_published;
         *
         *
         *
         *
         * Example syntax for PostgreSQL and SQLite
         *
                INSERT INTO the_table (id, column_1, column_2)
                VALUES (1, 'A', 'X')
                ON CONFLICT DO UPDATE
                  SET id = excluded.id,
                      column_1 = excluded.column_1,
                      column_2 = excluded.column_2;
         */
        
        // Now build UPDATE string
        $set_str_arr = [];
        foreach (array_keys($data) as $col) {
            $set_str_arr[] = "`{$col}` = :{$col}";
        }

        $mysqlDriver = false; // FIXME - detect
        if ($mysqlDriver) {
            $set_str = implode(", ", $set_str_arr);
            $stm .= " ON DUPLICATE KEY UPDATE {$set_str}";
        } else {
            $set_str = "SET " . implode(", ", $set_str_arr);
            $stm .= " ON CONFLICT DO UPDATE {$set_str}";
        }
        
        $count = $this->pdo->fetchAffected($stm, $data);

        // We should always have 1 row or 2 rows
        if ($count > 2) {
            throw new Exception("Expected to insert/update 1 or 2 entries in {$table}, but inserted/updated {$count}");
        }
        
        return $count;
    }
    
    
    /**
     * Performs an INSERT .. VALUES (...) ON DUPLICATE KEY UPDATE statement.
     * Used to insert multiple records at once.
     *
     * NOTE:(jcheney) The UPDATE will only be run if a duplicate is found based on a unique index.  And
     *                the only way we'll be able to find a unique match based on index is if the columns passed in
     *                are covered by a unique index.  So, for example, if you pass in all fields except an id, and the
     *                only unique field on the table IS the id, then you won't end up with updates, but instead with
     *                duplicate records.  Hopefully that makes sense.  Basically it just means, ensure your $data includes
     *                the columns necessary to be able to find a duplicate record so that it'll be updated, rather than
     *                inserting a new record.
     *
     * @param string $table name of table you want to insert into.
     * @param array  $data  Array of Arrays, where each sub-array is an array
     *                    of field => value pairs of the record you'd like to insert.
     *
     * @return int  Number of affected records.
     * @throws Exception
     */
    public function dbUpsertRecords(string $table, array $data): int
    {
        /*
         * Example syntax
         *
                INSERT INTO t
                    (t.a, t.b, t.c)
                VALUES
                    ('key1','key2','value1'),
                    ('key1','key3','value2'),
                    ('key1','key4','value3')
                ON DUPLICATE KEY UPDATE
                t.a = VALUES(t.a),
                t.b = VALUES(t.b),
                t.c = VALUES(t.c),

         *
         *
         *
         * Example syntax for PostgreSQL and SQLite
         *
                INSERT INTO the_table (id, column_1, column_2)
                VALUES (1, 'A', 'X'),
                       (2, 'B', 'Y'),
                       (3, 'C', 'Z')
                ON CONFLICT DO UPDATE
                  SET column_1 = excluded.column_1,
                      column_2 = excluded.column_2;
         */
         
        
        if (!$this->validatePdoConnection()) {
            throw new Exception("Database connection not initialized.");
        }
        
        // Build our columns portion of the statement.
        // Use the first element off of the array to do it.
        $cols = array_keys($data[0]);
        $cols = "`" . implode('`, `', $cols) . "`";
        
        // Build our value placeholders for a single row. I.e. (?, ?, ? .. n)
        $rowPlaceHolder = ' ('.implode(', ', array_fill(1, count($data[0]), '?')).')';
        
        // Build the whole prepared query
        $stm = "INSERT INTO `{$table}` ({$cols}) VALUES \n";
        $stm .= implode(", \n", array_fill(1, count($data), $rowPlaceHolder));
        
        // Build our UPDATE clause, using first element of our $data.
        $setStr = '';
        $mysqlDriver = false;
        if ($mysqlDriver) {
            foreach ($data[0] as $key => $val) {
                $setStr .= (empty($setStr) ? '' : ', ');
                $setStr .= "`{$key}` = VALUES(`{$key}`)\n";
            }
    
            $stm .= "\n ON DUPLICATE KEY UPDATE \n {$setStr}";
        } else {
            foreach ($data[0] as $key => $val) {
                $setStr .= (empty($setStr) ? '' : ', ');
                $setStr .= "`{$key}` = excluded.{$key}\n";
            }
            
            $stm .= "\n ON CONFLICT DO UPDATE SET {$setStr}";
        }
        
        $preparedStatement = $this->pdo->prepare($stm);
        
        // Flatten the value array (we are using ? placeholders)
        $values = [];
        foreach ($data as $rowData) {
            foreach ($rowData as $rowField) {
                $values[] = $rowField;
            }
        }
        
        // This will speed up the inserts
        $this->pdo->beginTransaction();
        
        $preparedStatement->execute($values);
        $count = $preparedStatement->rowCount();
        
        // Commit our transaction.
        $this->pdo->commit();
        
        return $count;
    }
}
